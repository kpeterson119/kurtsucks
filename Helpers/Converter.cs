﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiAssembler
{
    class Converter
    {
        public static string DecimalToHex(string input)
        {
            return ConvertBasedOnBases(input, 10, 16);
        }
        public static string DecimalToBinary(string input)
        {
            return ConvertBasedOnBases(input, 10, 2);
        }

        public static string HexToDecimal(string input)
        {
            return ConvertBasedOnBases(input, 16, 10);
        }
        
        public static string HexToBinary(string input)
        {
            return ConvertBasedOnBases(input, 16, 2);
        }
        
        public static string BinaryToDecimal(string input)
        {
            return ConvertBasedOnBases(input, 2, 10);
        }

        public static string BinaryToHex(string input)
        {
            return ConvertBasedOnBases(input, 2, 16);
        }

        private static string ConvertBasedOnBases(string input, int fromBase, int toBase)
        {
            return Convert.ToString(Convert.ToUInt32(input, fromBase), toBase);
        }

        public static UInt32 BinaryToUInt32(string binary)
        {
            return Convert.ToUInt32(binary, 2);
        }
    }
}