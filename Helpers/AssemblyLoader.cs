﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiAssembler
{
    public class AssemblyLoader
    {
        private static string FilePath = "C:\\Users\\Kurt Peterson\\Documents\\Visual Studio 2013\\Projects\\PiAssembler\\";
        public static string[] LoadBlinkingLight()
        {
            string file = @"Input/BlinkingLight.txt";
            string fullpath = FilePath + file;
            string[] lines = File.ReadAllLines(fullpath);
            return lines;
        }

        internal static string[] LoadHi()
        {
            string file = @"Input/Hi.txt";
            string fullpath = FilePath + file;
            string[] lines = File.ReadAllLines(fullpath);
            return lines;
        }
    }
}