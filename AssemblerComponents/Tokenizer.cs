﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiAssembler
{
    public class Tokenizer
    {
        public static Token Tokenize(string arguments)
        {
            Token newToken = new Token();
            
            string[] arguments2 = arguments.Split(',');

            switch(arguments2[0])
            {
                case "ADD":
                    ADD(arguments2, out newToken);
                    break;
                case "BAL":
                    BAL(arguments2, out newToken);
                    break;
                case "BL":
                    BL(arguments2, out newToken);
                    break;
                case "BNE":
                    BNE(arguments2, out newToken);
                    break;
                case "LABEL":
                    LABEL(arguments2, out newToken);
                    break;
                case "LDR":
                    LDR(arguments2, out newToken);
                    break;
                case "MOV":
                    MOV(arguments2, out newToken);
                    break;
                case "MOVT":
                    MOVT(arguments2, out newToken);
                    break;
                case "MOVW":
                    MOVW(arguments2, out newToken);
                    break;
                case "ORR":
                    ORR(arguments2, out newToken);
                    break;
                case "POP":
                    POP(arguments2, out newToken);
                    break;
                case "PUSH":
                    PUSH(arguments2, out newToken);
                    break;
                case "STR":
                    STR(arguments2, out newToken);
                    break;
                case "SUBS":
                    SUBS(arguments2, out newToken);
                    break;
                default:
                    throw new Exception();
            }
            return newToken;
        }

        private static void ADD(string[] stuff, out Token token)
        {
            //ADD,R,R,#
            token = new Token();
            token.OperationCode = Operation.Add;
            token.ConditionCode = Condition.Always;
            token.ToRegister = stuff[1];
            token.FromRegister = stuff[2];
            string imm12 = Converter.DecimalToBinary(stuff[3]);
            while (imm12.Length < 12)
            {
                imm12 = ("0" + imm12);
            }
            token.IMM12 = imm12;
            token.Store = false;
        }

        //no longer used
        private static void BAL(string[] stuff, out Token token)
        {
            //BAL,Label
            token = new Token();
            token.OperationCode = Operation.BranchAlways;
            token.ConditionCode = Condition.Always;
            token.Label = stuff[1];
            token.Store = false;
        }

        private static void BL(string[] stuff, out Token newToken)
        {
            newToken = new Token();
            newToken.ConditionCode = Condition.Always;
            newToken.OperationCode = Operation.BranchWithLink;
            newToken.Label = stuff[1];
        }

        //No longer used
        private static void BNE(string[] stuff, out Token token)
        {
            //BNE,#
            token = new Token();
            token.Label = stuff[1];
            token.OperationCode = Operation.BranchNotEqual;
            token.ConditionCode = Condition.NotEqual;
            token.Store = false;
        }

        private static void LABEL(string[] stuff, out Token token)
        {
            token = new Token();
            token.OperationCode = Operation.Label;
            string labelName = stuff[1];
            token.Label = labelName;
        }

        private static void LDR(string[] stuff, out Token token)
        {
            //LDR,R,R,#
            token = new Token();
            token.OperationCode = Operation.Load;
            token.ConditionCode = Condition.Always;
            token.FromRegister = stuff[2];
            token.ToRegister = stuff[1];
            string imm12 = Converter.DecimalToBinary(stuff[3]);
            while (imm12.Length < 12)
            {
                imm12 = ("0" + imm12);
            }
            token.IMM12 = imm12;
            token.Store = false;
            token.Write = false;
        }

        private static void MOV(string[] stuff, out Token token)
        {
            token = new Token();
            token.OperationCode = Operation.Move;
            token.ConditionCode = Condition.Always;
            token.FromRegister = stuff[2];
            token.ToRegister = stuff[1];
        }

        private static void MOVT(string[] stuff, out Token token)
        {
            //MOVT,R,#
            token = new Token();
            token.OperationCode = Operation.MoveT;
            token.ConditionCode = Condition.Always;
            token.ToRegister = stuff[1];
            string imm16 = Converter.DecimalToBinary(stuff[2]);
            while(imm16.Length < 16)
            {
                imm16 = ("0" + imm16);
            }
            token.IMM4 = imm16.Substring(0, 4);
            token.IMM12 = imm16.Substring(4);
            token.Store = false;
        }

        private static void MOVW(string[] stuff, out Token token)
        {
            //MOVW,R,#
            token = new Token();
            token.OperationCode = Operation.MoveW;
            token.ConditionCode = Condition.Always;
            token.ToRegister = stuff[1];
            string imm16 = Converter.DecimalToBinary(stuff[2]);
            while (imm16.Length < 16)
            {
                imm16 = ("0" + imm16);
            }
            token.IMM4 = imm16.Substring(0, 4);
            token.IMM12 = imm16.Substring(4);
            token.Store = false;
        }

        private static void ORR(string[] stuff, out Token token)
        {
            //ORR,R,R,#
            token = new Token();
            token.OperationCode = Operation.Or;
            token.ConditionCode = Condition.Always;
            token.FromRegister = stuff[1];
            token.ToRegister = stuff[2];
            string imm12 = Converter.DecimalToBinary(stuff[3]);
            while(imm12.Length < 12)
            {
                imm12 = ("0" + imm12);
            }
            token.IMM12 = imm12;
            token.Store = false;
        }

        private static void POP(string[] stuff, out Token token)
        {
            //POP,R
            token = new Token();
            token.OperationCode = Operation.Pop;
            token.ConditionCode = Condition.Always;
            token.FromRegister = stuff[1];
            token.Write = true;
            token.Store = false;
        }

        private static void PUSH(string[] stuff, out Token token)
        {
            token = new Token();
            token.OperationCode = Operation.Push;
            token.ConditionCode = Condition.Always;
            token.FromRegister = stuff[1];
            token.Write = true;
            token.Store = false;
        }

        private static void STR(string[] stuff, out Token token)
        {
            //STR,R,R,#
            token = new Token();
            token.OperationCode = Operation.Store;
            token.ConditionCode = Condition.Always;
            token.FromRegister = stuff[2];
            token.ToRegister = stuff[1];
            int temp = int.Parse(stuff[3]);
            string tempString = temp.ToString();
            string imm12 = Converter.DecimalToBinary(tempString);
            while(imm12.Length < 12)
            {
                imm12 = "0" + imm12;
            }
            token.IMM12 = imm12;
            token.Store = false;
        }

        private static void SUBS(string[] stuff, out Token token)
        {
            //SUBS,R,R,#
            token = new Token();
            token.OperationCode = Operation.Subtract;
            token.ConditionCode = Condition.Always;
            token.FromRegister = stuff[1];
            token.ToRegister = stuff[2];
            string imm12 = Converter.DecimalToBinary(stuff[3]);
            while(imm12.Length < 12)
            {
                imm12 = ("0" + imm12);
            }
            token.IMM12 = imm12;
            token.Store = true;
        }
    }
}