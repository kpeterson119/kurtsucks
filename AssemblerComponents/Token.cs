﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiAssembler
{
    public class Token
    {
        public Operation OperationCode { get; set; }
        public Condition ConditionCode { get; set; }
        public string FromRegister { get; set; }
        public string ToRegister { get; set; }
        public bool Store { get; set; }
        public string IMM4 { get; set; }
        public string IMM12 { get; set; }
        public string IMM24 { get; set; }
        public string Label { get; set; }
        public bool Write { get; set; }
    }
}