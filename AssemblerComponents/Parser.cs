﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiAssembler
{
    public class Parser
    {
        public static string Parse(Token input)
        {
            string binary = "";
            string S = (input.Store) ? "1" : "0";
            string condition = ParseCondition(input.ConditionCode);

            switch(input.OperationCode)
            {
                case Operation.Add:
                    binary = ParseAdd(input, condition, S);
                    break;
                case Operation.BranchAlways:
                    binary = ParseBranchAlways(input, condition);
                    break;
                case Operation.BranchNotEqual:
                    binary = ParseBranchNotEqual(input, condition);
                    break;
                case Operation.BranchWithLink:
                    binary = ParseBranchWithLink(input, condition);
                    break;
                case Operation.Load:
                    binary = ParseLoad(input, condition);
                    break;
                case Operation.Move:
                    binary = ParseMove(input, condition);
                    break;
                case Operation.MoveT:
                    binary = ParseMoveT(input, condition);
                    break;
                case Operation.MoveW:
                    binary = ParseMoveW(input, condition);
                    break;
                case Operation.Or:
                    binary = ParseOr(input, condition, S);
                    break;
                case Operation.Pop:
                    binary = ParsePop(input, condition);
                    break;
                case Operation.Push:
                    binary = ParsePush(input, condition);
                    break;
                case Operation.Store:
                    binary = ParseStore(input, condition);
                    break;
                case Operation.Subtract:
                    binary = ParseSubtract(input, condition, S);
                    break;
            }

            return binary;
        }

        private static string ParseAdd(Token input, string condition, string S)
        {
            string from = ParseRegister(input.FromRegister);
            string to = ParseRegister(input.ToRegister);
            //     cond       00   1   0100  S    Rn      Rd    imm12
            return(condition+"00"+"1"+"0100"+S+""+from+""+to+""+input.IMM12);
        }

        private static string ParseBranchAlways(Token input, string condition)
        {
            //     cond       1010  imm24
            return(condition+"1010"+input.IMM24);
        }

        private static string ParseBranchNotEqual(Token input, string condition)
        {
            //     cond       1010  imm24
            return(condition+"1010"+input.IMM24);
        }

        private static string ParseBranchWithLink(Token input, string condition)
        {
            //      cond         1011    imm24   
            return (condition + "1011" + input.IMM24);
        }

        private static string ParseLoad(Token input, string condition)
        {
            string P = "1";
            string U = "1";
            string W = ((input.Write)?1:0).ToString();
            string from = ParseRegister(input.FromRegister);
            string to = ParseRegister(input.ToRegister);
            //      cond       010  P    U  0  W  1  Rn      Rt    imm12
            return (condition+"010"+P+""+U+"0"+W+"1"+from+""+to+""+input.IMM12);
        }

        private static string ParseMove(Token input, string condition)
        {
            string to = ParseRegister(input.ToRegister);
            string from = ParseRegister(input.FromRegister);
            string S = "0";
            //      cond         00     0     1101    S    0000    Rd    00000000    Rm
            return (condition + "00" + "0" + "1101" + S + "0000" + to + "00000000" + from);
        }

        private static string ParseMoveT(Token input, string condition)
        {
            string to = ParseRegister(input.ToRegister);
            //     cond       0011   0100  imm4          Rd                  imm12
            return(condition+"0011"+"0100"+input.IMM4+""+to+""+input.IMM12);
        }

        private static string ParseMoveW(Token input, string condition)
        {
            string to = ParseRegister(input.ToRegister);
            //      cond         0011     0000    imm4              Rd        imm12
            return (condition + "0011" + "0000" + input.IMM4 + "" + to + "" + input.IMM12);
        }

        private static string ParseOr(Token input, string condition, string S)
        {
            string from = ParseRegister(input.FromRegister);
            string to = ParseRegister(input.ToRegister);
            //      cond       00   1   1100  S    Rn      Rd    imm12
            return (condition+"00"+"1"+"1100"+S+""+from+""+to+""+input.IMM12);
        }

        private static string ParsePop(Token input, string condition)
        {
            string from = ParseRegister(input.FromRegister);
            //     cond       010010011101  from  000000000100
            return(condition+"010010011101"+from+"000000000100");
        }

        private static string ParsePush(Token input, string condition)
        {
            string from = ParseRegister(input.FromRegister);
            //      cond       010100101101  from  000000000100
            return (condition+"010100101101"+from+"000000000100");
        }

        private static string ParseStore(Token input, string condition)
        {
            string from = ParseRegister(input.FromRegister);
            string to = ParseRegister(input.ToRegister);
            string P = "1";
            string U = "1";
            string W = ((input.Write) ? 1 : 0).ToString();
            //      cond       010  P    U  0  W  0  Rn      Rt    imm12
            string temp = (condition+"010"+P+""+U+"0"+W+"0"+from+""+to+""+input.IMM12);
            return temp;
        }

        private static string ParseSubtract(Token input, string condition, string S)
        {
            string from = ParseRegister(input.FromRegister);
            string to = ParseRegister(input.ToRegister);
            //      cond       00   1   0010  S    Rn      Rd    imm12
            return (condition+"00"+"1"+"0010"+S+""+from+""+to+""+input.IMM12);
        }

        public static string ParseCondition(Condition cond)
        {
            string binary = "";

            switch(cond)
            {
                case Condition.Always:
                    binary = "1110";
                    break;
                case Condition.Equal:
                    binary = "";
                    break;
                case Condition.GE:
                    binary = "";
                    break;
                case Condition.GT:
                    binary = "";
                    break;
                case Condition.LE:
                    binary = "";
                    break;
                case Condition.LT:
                    binary = "";
                    break;
                case Condition.NotEqual:
                    binary = "0001";
                    break;
            }
            return binary;
        }

        public static string ParseRegister(string register)
        {
            string tempNumber = register.Substring(1);
            string reg = Converter.DecimalToBinary(tempNumber);
            while(reg.Length < 4)
            {
                reg = ("0" + reg);
            }
            return reg;
        }

        private static string ParseRegistersString(string register)
        {
            string temp = register.Remove(0, 1);
            int regNumber = int.Parse(temp);
            string stringy = "";
            for(int i = 0; i < regNumber; i++)
            {
                stringy = stringy+"0";
            }
            stringy = "1" + stringy;

            while(stringy.Length < 16)
            {
                stringy = "0" + stringy;
            }
            return stringy;
        }
    }
}