﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiAssembler.AssemblerComponents
{
    public class Assembler
    {
        private static string pathToSave = @"C:\\Users\\Kurt Peterson\\Documents\\Visual Studio 2013\\Projects\\PiAssembler\\Output\\kernel7.img";
        public static void Assemble(string[] commandStrings)
        {
            //Tokenize
            Token[] tokens = new Token[commandStrings.Length];
            for (int i = 0; i < commandStrings.Length; i++)
            {
                Token currentToken = Tokenizer.Tokenize(commandStrings[i]);
                tokens[i] = currentToken;
            }

            //parse labels
            int labelCount = 0;
            Dictionary<string, int> commandNameIndexMapping = new Dictionary<string, int>();
            for (int i = 0; i < tokens.Length; i++)
            {
                if(tokens[i].OperationCode == Operation.Label)
                {
                    commandNameIndexMapping.Add(tokens[i].Label, (i-labelCount));
                    labelCount++;
                }
            }

            int offset = 0;
            string[] binaryStrings = new string[commandStrings.Length - labelCount];
            Operation[] branchOps = new Operation[] { Operation.BranchNotEqual, Operation.BranchAlways, Operation.BranchWithLink };
            for (int i = 0; i < tokens.Length; i++ )
            {
                Token current = tokens[i];
                if(branchOps.Contains(current.OperationCode))
                {
                    uint labelIndex = (uint)commandNameIndexMapping[current.Label];
                    uint branchIndex = (uint)i - (uint)offset;
                    uint distance = labelIndex - branchIndex;

                    uint location = distance - 2;

                    string imm24 = Converter.DecimalToBinary(location.ToString());
                    while (imm24.Length < 24)
                    {
                        imm24 = ("0" + imm24);
                    }
                    while(imm24.Length>24)
                    {
                        imm24 = imm24.Substring(1);
                    }
                    current.IMM24 = imm24;

                    string binary = Parser.Parse(current);
                    binaryStrings[i - offset] = binary;
                }
                else if(current.OperationCode == Operation.Label)
                {
                    offset++;
                }
                else
                {
                    string binary = Parser.Parse(current);
                    binaryStrings[i - offset] = binary;
                }
            }

            //parse binary to Uint
            int imp = 0;
            List<UInt32> uints = new List<UInt32>();
            foreach (string binary in binaryStrings)
            {
                string temp = binary;
                while (binary.Length < 32)
                {
                    temp = "0" + temp;
                }
                string reordered = Reorder(temp);
                UInt32 intToWrite = Converter.BinaryToUInt32(reordered);
                uints.Add(intToWrite);
                imp++;
            }

            //prep bytes for writing
            byte[] decoded = new byte[uints.Count * sizeof(UInt32)];
            for(int o = 0; o < uints.Count; o++)
            {
                byte[] byteArray = BitConverter.GetBytes(uints[o]);
                for(int i = 0; i < byteArray.Count(); i++)
                {
                    decoded[(o*4)+i] = byteArray[(byteArray.Count()-1)-i];
                }
            }

            //write bytes to file
            using (FileStream fs = File.Create(pathToSave))
            {
                fs.Write(decoded, 0, decoded.Length);
            }
        }

        private static string Reorder(string input)
        {
            string[] sections = new string[4];
            sections[0] = input.Substring(0, 8);
            sections[1] = input.Substring(8, 8);
            sections[2] = input.Substring(16, 8);
            sections[3] = input.Substring(24, 8);
            string reordered = (sections[3] + "" + sections[2] + "" + sections[1] + "" + sections[0]);
            return reordered;
        }
    }
}