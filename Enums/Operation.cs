﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiAssembler
{
    public enum Operation
    {
        Add, Subtract, MoveT, MoveW, Or, Store, Load, Pop, Push, Label, Move, BranchWithLink, BranchNotEqual, BranchAlways
    }
}