﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiAssembler
{
    public enum Condition
    {
        Equal, NotEqual, LT, LE, GT, GE, Always
    }
}