﻿using PiAssembler.AssemblerComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiAssembler
{
    class Program
    {
        static void Main(string[] args)
        {
            //ExecuteBlinkingLight();
            ExecuteHi();
        }

        private static void ExecuteBlinkingLight()
        {
            string[] commandStrings = AssemblyLoader.LoadBlinkingLight();
            Assembler.Assemble(commandStrings);
        }

        private static void ExecuteHi()
        {
            string[] commandStrings = AssemblyLoader.LoadHi();
            Assembler.Assemble(commandStrings);
        }
    }
}